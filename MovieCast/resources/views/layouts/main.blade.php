<!DOCTYPE html>
<html lang="en">

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MovieCast</title>
    
    <link rel="stylesheet" href="/css/main.css">
</head >
<body   style = "margin: 0px; background-color: #333b36;"  > 

    <nav id="nave"  style = "background-image: linear-gradient(#000000, #333b36); display: flex; box-shadow: 1px 1px 2px; ">
  <div style =  "width: 100%; padding: 5px; margin: 15px;  ">
  
  <a style = "color: #fff; margin-right: 10px; font-weight: 100; box-shadow: 1px 1px 1px; border: 1px;
  padding: 5px; border-radius: 25px;" href="#" id = "a1">Movies</a>
  <a  style = "color: #fff;margin-right: 10px;font-weight: 100; box-shadow: 1px 1px 1px; border: 1px;
  padding: 5px; border-radius: 25px; " href="#" id = "a2">TV Shows</a>
  <a  style = "color: #fff; margin-left: 10px;font-weight: 100; box-shadow: 1px 1px 1px; border: 1px;
  padding: 5px; border-radius: 25px;" href="#" id = "a3" >Actors</a>
  
  </div>
    <div style="width: auto; display: flex; text-align: right; justify-content: right; align-items: right; padding: 15px; ">
    <input type="search" name="s" id="s"  placeholder="Search" style="background-color: rgb(46, 42, 42); 
    border-radius: 25px; color: #fff; padding: 5px; outline: none; box-shadow: 1px 1px 2px;">
    </div>
    </nav>
    
    @yield('content')
    
    
    <script src="/js/app.js"></script>
</body>
</html>